import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { DemoComponent } from './demo/demo.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase } from 'angularfire2/database';
import {HttpModule} from "@angular/http";
import { FirebasedemoComponent } from './firebasedemo/firebasedemo.component';
export const firebaseconfig={
    apiKey: "AIzaSyDrIVRxm0b5VwzVUeGBXQL8Hxp0x9TKSCE",
    authDomain: "myproject-27ae5.firebaseapp.com",
    databaseURL: "https://myproject-27ae5.firebaseio.com",
    projectId: "myproject-27ae5",
    storageBucket: "myproject-27ae5.appspot.com",
    messagingSenderId: "685963829552"

}


@NgModule({
  declarations: [
    AppComponent,
    DemoComponent,
    FirebasedemoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(firebaseconfig)
  ],
  providers: [AngularFireDatabase],
  bootstrap: [AppComponent]
})
export class AppModule { }
