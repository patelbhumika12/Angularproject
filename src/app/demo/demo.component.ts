import { Component, OnInit } from '@angular/core';
import { Task } from "./Task";
@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {
  no1:number=0;
  no2:number=0;
  ans:number=0;
  Id:string='';
  Title:string='';
  Status:string='';

    arr:Task[]=[
    new Task('1','email to manager','done'),
    new Task('2','push your code on github','pending')
    ];
  constructor() { }

  ngOnInit() {
  }
  onDelete(item)
  {
    this.arr.splice(this.arr.indexOf(item),1);

  }
  onAdd()
  {
    this.ans=this.no1 +this.no2;

  }
  onAdd1()
  {
this.arr.push(new Task(this.Id,this.Title,this.Status));
this.Id='';
this.Title='';
this.Status='';

  }

}
