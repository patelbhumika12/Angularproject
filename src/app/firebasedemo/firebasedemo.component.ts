import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase,FirebaseListObservable } from "angularfire2/database";

@Component({
  selector: 'app-firebasedemo',
  templateUrl: './firebasedemo.component.html',
  styleUrls: ['./firebasedemo.component.css']
})
export class FirebasedemoComponent implements OnInit {
Pname:string='';
Pprice:number=0;
Pimg:string='';
arr:FirebaseListObservable<any[]>;
  constructor(public obj:AngularFireDatabase) { }

  ngOnInit() {
    this.arr=this.obj.list('/products');
  }
  onAdd(){
    this.arr.push({Pname:this.Pname,Pprice:this.Pprice,Pimg:this.Pimg});
  }
  onDelete(x){
    this.arr.remove(x);
  }

}
